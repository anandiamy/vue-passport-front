import Vue         from 'vue'
import App         from './App.vue'
import Router      from './routes'
// import Vuetify     from 'vuetify'
import Auth        from './packages/auth/Auth'
import VeeValidate from 'vee-validate'
import store       from './store'
import { apiHost } from './packages/config'
import VueAxios    from 'vue-axios'
import axios       from 'axios'

// Vue.use(Vuetify)
Vue.use(Auth)
Vue.use(VeeValidate)
Vue.use(VueAxios, axios)

axios.defaults.baseURL = apiHost
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

axios.interceptors.request.use(config => {
  config.headers.authorization = 'Bearer ' + Vue.auth.getToken()
  return config
}, err => {
  Promise.reject(error)
});

axios.interceptors.response.use(response => {
  return response
}, function (error) {
  if (error.response) {
    if (error.response.status === 404)
      swal(error.response.status.toString(), error.response.data.error, 'error')
    else if (error.response.status === 500)
      swal(error.response.status.toString(), "We are experincing a problem in our servers", 'error')
  }
  return Promise.reject({ error });
});

Router.beforeEach(
    (to, from, next) => {
      if (to.matched.some(record => record.meta.forVisitors)){
        if (Vue.auth.isAuthenticated()){
          next({
            path: '/feed'
          })
        }else next()
      }

      else if (to.matched.some(record => record.meta.forAuth)){
        if( ! Vue.auth.isAuthenticated() ){
          swal('Ooops', "You must logged in to access that page", 'error')
          next({
            path: '/login'
          })
        }else next()
      }

      else next()
    }
)

new Vue({
  el: '#app',
  store,
  render: h => h(App),
  router: Router
})
