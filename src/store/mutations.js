import * as mutations from './mutation-types'

export default {
  [mutations.SHOW_PRODUCT] (state, product2) {
    state.singleView = 'true'
  },
  [mutations.IS_AUTH] (state, status) {
    state.isAuth = status
  }
}