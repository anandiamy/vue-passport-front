import * as mutations from './mutation-types'

export default {
  showProduct(context, product2) {
    //manggil mutation
    context.commit(mutations.SHOW_PRODUCT, product2)
  },
  isAuth(context, status) {
    context.commit(mutations.IS_AUTH, status)
  }
}