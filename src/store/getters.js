export default {
  products2: state => {
    return state.products2
  },
  singleView: state => {
    return state.singleView
  },
  isAuth: state => {
    return state.isAuth
  }
}